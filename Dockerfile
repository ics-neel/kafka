FROM python:3.11-alpine
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# Install virtualenv
RUN pip install virtualenv

# Create a virtual environment
RUN python -m virtualenv $VIRTUAL_ENV
WORKDIR /app


RUN pip install flask

COPY . .

CMD ["python", "app.py"]
