from confluent_kafka import Producer as pd
import json
import sys



# Kafka broker
bootstrap_servers = 'localhost:9092'

def delivery_callback(err, msg):
    if err:
        print('Message delivery failed: {}'.format(err))
    else:
        print("Sent")
        # print('Message delivered to neel {} [{}]'.format(msg.topic(), msg.partition()))

def produce_message(producer, topic, key, value):
    producer.produce(topic, key=key, value=value, callback=delivery_callback)
    producer.poll(0)

def main():
    # Create Kafka producer
    producer = pd({'bootstrap.servers': bootstrap_servers})

    # Define topic
    topic = 'test'
    # if len(sys.argv) < 2:
    #     # print("Usage: python_script.py <url>")
    #     return
    url = 'https://example.com'
    key = None
    produce_message(producer, topic, key, url)
    # Send messages
    # for i in range(1):
    #     message = {'message': 'Hello Kafka! Neel Message {}'.format(i)}
    #     key = None  # You can specify a key if you want
    #     value = json.dumps(message)
    #     produce_message(producer, topic, key, value)

    # Flush producer
    producer.flush()

if __name__ == '__main__':
    main()
