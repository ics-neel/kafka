# import subprocess

# def run_another_python_file(file):
#     try:
      
#         subprocess.run(['python', file], check=True)
#     except subprocess.CalledProcessError as e:
#         print(f"Error running the script: {e}")
# file1='producer.py'
# file2='consumer.py'

# run_another_python_file(file1)
# run_another_python_file(file2)


import subprocess


file1='producer.py'
file2='consumer.py'
url = "https://example.com"

def run_another_producer(file,url):
    try:
        subprocess.run(['python', file,url], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error running the script: {e}")

        
def run_another_consumer(file):
    try:
      
        subprocess.run(['python', file], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error running the script: {e}")

run_another_producer(file1,url)
run_another_consumer(file2)