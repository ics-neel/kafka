import sys

def main():
    # Get the URL string from command-line arguments
    if len(sys.argv) < 2:
        print("Usage: python_script.py <url>")
        return
    url = sys.argv[1]
    print(url)




    # Your code here, using the 'url' variable

if __name__ == "__main__":
    main()