from confluent_kafka import Consumer, KafkaError

# Kafka broker
bootstrap_servers = 'localhost:9092'

def consume_messages(consumer, topic):
    consumer.subscribe([topic])

    while True:
        msg = consumer.poll(1.0)  # Adjust timeout to 0.1 seconds

        if msg is None:
            continue
        if msg.error():
            if msg.error().code() == KafkaError._PARTITION_EOF:
                # End of partition
                continue
            else:
                print(msg.error())
                break

        # print('Received message hello: {}'.format(msg.value().decode('utf-8')))
        url=msg.value().decode('utf-8')
        print(url)
        break  # Break after processing one message

    consumer.close()

def main():
    
        # Create Kafka consumer
    consumer = Consumer({
        'bootstrap.servers': bootstrap_servers,
        'group.id': 'my_consumer_group',
        'auto.offset.reset': 'earliest'
    })

    # Define topic
    topic = 'test'

    # Consume messages
    consume_messages(consumer, topic)

if __name__ == '__main__':
    
    main()